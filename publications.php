<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Dr. Shamsun Nahar</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
 </head>

<body>
<div  class="bdy"><table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><a class="thumbnail" href="#thumb"><img src="images/name.jpg" border="0" /><span class="style3"><img src="images/nahardr.jpg" /><br />Dr. MS Nahar</span></a></td>
  </tr>
  <tr>
    <td colspan="3" align="right"><hr style="color:#00CCFF; padding:1px; height:5px; background-color:#00CCFF;" />
    <?php 
			include('main_nav.php');
		?>
    </td>
  </tr>
  <tr>
    <td width="126" align="center"><img src="images/nahar.jpg" width="82" height="110" /></td>
    <td width="512"> <?php 
		include('address.php');
	?></td>
    <td width="142" align="center"></td>
  </tr>
  
  
  <tr>
    <td colspan="3" align="center"><hr /></td>
  </tr>
  
  <tr>
    <td colspan="2" align="center"><div align="left"><span class="style4">List of publication</span></div></td>
    <td width="142" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="style3">
    <div class="publicationBox">
    &nbsp;
    <p>Refereed international journal publications</p>
<p>2011</p>
<ul>
  <li>Mst. Shamsun Nahar, Jing ZHANG, Concentration and  Distribution of Organic and Inorganic Water Pollutants in Eastern Shizuoka,  Japan, <em>Toxicological &amp; Environmental Chemistry</em> (Article in press, 2011). </li>
  <li>Mst. Shamsun  Nahar, Jing ��ZHANG, <a href="http://www.sciencedirect.com/science?_ob=ArticleURL&_udi=B9D0J-4XW6YYB-30&_user=104125&_coverDate=12%2F30%2F2009&_alid=1617508672&_rdoc=7&_fmt=high&_orig=search&_origin=search&_zone=rslt_list_item&_cdi=62096&_sort=r&_st=4&_docanchor=&_ct=4357&_acct=C000008041&_version=1&_urlVersion=0&_userid=104125&md5=bc8ace4276de2c052da31d9e839cdaf9&searchtype=a">Analysis of damaged silicon rubber hose</a>, <em>American Journal of Analytical Chemistry</em><em>,</em>2011, Vol. 2, pp. 363-370. </li>
  <li>Mst. Shamsun Nahar, Jing ZHANG, Assessment of sources variation in  potable water quality including organic, inorganic and trace  metals, <em>Environmental </em></li>
</ul>
<p><em>Geochemistry  and Health</em>(DOI  10.1007/s10653-011-9397-z). <br>
  2010</p>
<ul>
  <li>Mst. Shamsun Nahar,  Jing ZHANG, Influence of biogeochemical qualities of Shizuoka water on the degradation of PVC shower  hose, <em>J</em><em>. Envir</em><em>on. Protect</em><em>.</em>2011, Vol. 2, pp. 204-212. </li>
</ul>
<p>2011<br>
  <em>Articles (Submitted  to Journal)</em></p>
<ul>
  <li>Mst. Shamsun Nahar,  Jing ZHANG, Natural  Sources and Thresholds of Taste in Toyama  Water, (<em>Submitted</em>). </li>
  <li>Mst. Shamsun Nahar, Jing ZHANG, An introduction to the New Degradation of  Polyvinyl  Chloride (PVC) Shower Hose in Kumamoto Prefecture, Japan,� (<em>Submitted</em>) </li>
</ul>
<p>2009</p>
<ul>
  <li>Mst. Shamsun  Nahar, Kiyoshi Hasegawa, Shigehiro Kagaya, and Shigeyasu Kuroda,  Adsorption and Aggregation of Iron-Hydroxy Complexes During the  Photodegradation of Phenol Using the Iron-Added-TiO2 Combined  System,<em>J.  Hazard. Mater</em>Vol.162, pp. 351�355, February 2009. </li>
  <li>Mst. Shamsun Nahar,  Jing ZHANG, Kiyoshi Hasegawa, Shigehiro Kagaya, Shigeyasu Kuroda, Phase  Transformation of Anatase�Rutile Crystals in Doped and Undoped TiO2  Particles Obtained by the Oxidation of Polycrystalline Sulfide, <em>Mat</em><em>er.</em><em> Sci</em><em>. </em><em>�Semi</em><em>.</em><em> Proces</em><em>s,</em>12 (2009) 168-174. </li>
</ul>
<p>2007</p>
<ul>
  <li>Mst. Shamsun Nahar,  Kiyoshi Hasegawa, Shigehiro Kagaya, and Shigeyasu Kuroda, Comparative  Assessment of the Efficiency of Fe-doped TiO2 Prepared by Two Doping  Methods and Photocatalytic Degradation of Phenol in Domestic Water Suspensions, <em>Sci.�  Technol. Adv. Mater.</em>, Vol. 8, pp. 286-291, May 2007.</li>
  <li>Mst. Shamsun  Nahar, Kiyoshi Hasegawa, Shigehiro  Kagaya, and Shigeyasu Kuroda, Degradation of Phenol under Visible Light  Irradiation using TiO2 with Fe(III) and Easy Sedimentation of TiO2  particle, <em>Bull. Chem. Soc. Jpn.</em>, Vol. 80, pp.  1017-1019, May 2007. </li>
  <li>Kiyoshi Hasegawa, Yosuke Ohki, Kenji Izawa, Mst. Shamsun Nahar, and  Shigehiro Kagaya, Enhanced Degradation of Phenol in an Electrolyte-Containing  Model Wastewater Using a Combined Photocatalyst of TiO2 and Fe(ClO4)3  in a Continuous Flow-Type Shallow Photoreactor Combined with Coagulation of TiO2, <em>J.  Ecotech. Res.</em> Vol. 13, pp. 21-27, May 2007.</li>
</ul>
<p>2006</p>
<ul>
  <li>Mst. Shamsun  Nahar, Kiyoshi Hasegawa, Shigehiro Kagaya, Photocatalytic Degradation of Phenol by  Visible Light-Responsive Iron-Doped TiO2 and Spontaneous  Sedimentation of the TiO2 Particles. <em>Chemosphere,</em> Vol. 65, pp. 1976-1982, June 2006. </li>
  <li>F. Islam, D. A. Begum and M. S. Nahar,  Sodium Sulfide Roasting of Waste Reformer Nickel Catalyst and Subsequent  Leaching with Sulfuric Acid<em>, Journal of  Bangladesh Academy Science</em>, Vol. 19, pp. 193-200, 1995. </li>
</ul>
<p>Refereed  international conference papers</p>
<p>2011</p>
<ul>
  <li>Mst.  Shamsun Nahar, Jing ZHANG,  Toxic Metals Separation from Drinking Water by Neem Leaves, 1st International  Conference on &ldquo;Environment, Genes, Health and Disease 2011� (EGHD&rsquo;2011)&rdquo; 9th-11th  December&rsquo;2011, Bharathiar University, Tamil Nadu, India.&nbsp; </li>
  <li>Mst.  Shamsun Nahar, Jing ZHANG, Natural Sources and Thresholds of Taste and� Flavour in Drinking Water, Japan, <em>Pangborn Symposium,</em> 4-8  September 2011, Toronto, Canada. </li>
  <li>Mst. Shamsun  Nahar, Jing ZHANG, Spatial Distribution of Aquatic 1, 4-dioxane  in Relation to Possible Sources in Paper Industrial Cities in Eastern Shizuoka,  Japan, <em>Environmental Health 2011</em><em>, </em><em>� Resetting Our Priorities</em>, 6-9 February 2011, Salvador, Brazil. </li>
  <li>Mst. Shamsun Nahar, Jing ZHANG, Investigation  Procedure of Unknown Physical Changes in Shower Hoses by Assessing the  Bio-Physico-Chemical Qualities of Eastern Shizuoka Water, <em>International Conference on Biomaterials Science</em><em> (ICBS)</em>,� Japan, March 15th� - 18th, 2011. </li>
</ul>
<p>2010</p>
<ul>
  <li>Mst. Shamsun Nahar, Jing ZHANG, <a href="http://www.sciencedirect.com/science?_ob=ArticleURL&_udi=B6V73-4M6RYT1-1&_user=104125&_coverDate=12%2F31%2F2006&_alid=1358472245&_rdoc=15&_fmt=high&_orig=search&_cdi=5831&_sort=r&_st=4&_docanchor=&_ct=625760&_acct=C000008041&_version=1&_urlVersion=0&_userid=104125&md5=a1676913c9ec0072ff876a58f27103ed">Assessment of Sources Variations in Potable Water Quality</a> Model  Including Organic, Inorganic and Trace Metals Constituents, <em>17th Asian symposium on ecotechnology</em>, Kurobe, Japan,  November 2010,� pp. 27.</li>
</ul>
<p>2009</p>
<ul>
  <li>Mst. Shamsun Nahar, Jing ZHANG, Natural Sources and Thresholds of Taste in  &ldquo;Tasty&rdquo; Water in Toyama, <em>?</em><em>3 </em><em>?????????????????</em><em>?????</em><em>?</em><em>,</em> (University of Toyama), 2009 ?12 ?25 ?.</li>
</ul>
<p>2007</p>
<ul>
  <li>Mst. Shamsun Nahar,  Kiyoshi Hasegawa, Shigehiro Kagaya, Degradation of Phenol under Visible Light  Irradiation using TiO2 with Fe(III) and Easy Sedimentation of TiO2  particle. <em>87th Spring  Annual Meeting of the Chemical Society of Japan</em>, pp. 192, March 2007. </li>
  <li>Mst. Shamsun Nahar, Kiyoshi Hasegawa, Shigehiro Kagaya, and Shigeyasu Kuroda,  Relationship concerning the adsorption and photoirradiation of iron-hydroxy  complexes on the surface of TiO2 and photocatalytic activity of  iron-TiO2 combined system. <em>Joint  meeting of the 1st Asian-Oceanian Conferrence on Green and  Sustainable Chemistry and the 7th Annual Green and Sustainable  Chemistry Symposium (GSCN-AON)</em>,  pp. 188, March 2007<em>.</em></li>
</ul>
<p>2006 </p>
<ul>
  <li>Mst. Shamsun  Nahar, Kiyoshi Hasegawa, Shigehiro Kagaya, Characteristics and  catalytic properties of Fe-doped TiO2�particles in aqueous photooxidation.<em>JAIST International Symposium (NT2006),</em> pp. 84-85, September 2006. </li>
</ul>
<p>2004 </p>
<ul>
  <li>Mst. Shamsun  Nahar, Kiyoshi Hasegawa, Kagaya  Shigehiro, Photocatalytic degradation of phenol by visible ray-responsive  Fe-doped titanium dioxide and spontaneous sedimentation of the particles.<em> 84th  Spring Annual Meeting of the Chemical Society of Japan</em>,pp. 516, March 2004.</li>
</ul>
<p>2000</p>
<ul>
  <li>Mst. Shamsun  Nahar, Higher dissolution of Nickel  from Roasted refractory waste Nickel Catalyst&rdquo; <em>7th West Bengal Sate science and Technological Congress India,</em> pp.  16, February 2000.</li>
  <li>M. M. Haque, A. S. M. A. Haseeb and M. S. Nahar, Studies on conducting Polymers. <em>Sixth west Bangle  State science and Technology Congress India,</em> pp.  16, February 1999.</li>
</ul>
<pre><em>Books </em></pre>
<pre>1. Mst. Shamsun Nahar et al., &ldquo;Environmental chemistry&rdquo;, for M.Sc and B.Sc (Honours) level (Bangladesh National Publisher: Bangla Academy).ISBN 984-07-4287-6, page 1-142, 2002.</pre>
<pre>2. Mst. Shamsun Nahar et al., &ldquo;Science and Technology related activities in Bangladesh&rdquo; (ISBN 984-31-0745-4) published Bangladesh National Scientific and Technical Documentation Center.</pre>
<p><em>Reviewer</em><em> of the journal</em><br>
  2007-2011<em> </em></p>
 
      <ul>
        <li>Journal of Hazardous Materials</li>
        <li>Materials Science  and Engineering </li>
      </ul>
     
<p><em>Member:</em>Chemical society of JAPAN.</p>

</div>    
    
</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
  <tr>
    <td height="35" colspan="3" align="center" bgcolor="#0099FF" class="style3">
    <?php 
		include('footer.php');
	?>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
</table>
</div>
 </body>
</html>
