<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Dr. Shamsun Nahar | Research</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div  class="bdy"><table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><a class="thumbnail" href="#thumb"><img src="images/name.jpg" border="0" /><span class="style3"><img src="images/nahardr.jpg" /><br />Dr. MS Nahar</span></a></td>
  </tr>
  <tr>
    <td colspan="3" align="right"><hr style="color:#00CCFF; padding:1px; height:5px; background-color:#00CCFF;" />
    <?php 
			include('main_nav.php');
		?>
    </td>
  </tr>
  <tr>
    <td width="126" align="center"><img src="images/nahar.jpg" width="82" height="110" /></td>
    <td width="512"> <?php 
		include('address.php');
	?></td>
    <td width="142" align="center"></td>
  </tr>
  
  
  <tr>
    <td colspan="3" align="center"><hr /></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><div align="left"></div></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><div align="left" class="style4"><strong><u>RESEARCH INTEREST</u></strong></div></td>
  </tr>
  <tr>
    <td colspan="3" align="left" class="style3">
    <div class="publicationBox">
    <p><em><u>Research G</u></em><em><u>rants</u></em><em><u> </u></em></p>
<ul>
  <li><strong>�(</strong><strong></strong><strong>)</strong></li>
</ul>
<p>October /2009-  Mar /2011</p>
<ul>
  <li>Research grant from TOTO and TOYOX Com. Ltd. </li>
</ul>
<p>Apr /2009- October  2009<br>
  Field of study to be specialized in the past and present<br>
</p>
<pre><strong><em><u>Present Posdoctoral Research: 2008-2011</u></em></strong></pre>
<pre><strong><em><u>&nbsp;</u></em></strong></pre>
<pre><strong>Reseach highlight </strong></pre>
<pre><strong>Hazardous Materials Science (Damaged PVC &amp; silicon rubber hose) </strong></pre>
<pre>Some aspects of chemicals effects on silicon polymer chain and degradation� mechanism of� silicon rubber hose by fluid attack  </pre>
<pre>An Introduction to the new degradation of polyvinyl chloride (PVC) shower hose  </pre>
<pre><strong>&nbsp;</strong></pre>
<pre><strong>Analytical and Environmental Water Science </strong></pre>
<pre>&nbsp;</pre>
<ul>
  <li>Characterization and application of herbal materials for resin preparation </li>
  <li>Determination of the optimum pH for the adsorption of As and Hg on the NOBIAS Chelate-PA1 resin </li>
  <li>Continuous separation of multi trace metal in seawater using new NOBIAS Chelate-PA1 resin </li>
</ul>
<pre>A standard formula for making artificial tasty water  </pre>
<pre>Palatable and unpalatable mineralization affect the drinking water quality  </pre>
<pre>Assessing the bio-physico-chemical qualities of Shizuoka water  </pre>
<pre>Determination of heavy and trace poisonous metals in water sources by ICP-MS </pre>
<pre>&nbsp;</pre>
<p><strong><em><u>Joint research program</u></em></strong><strong><em><u>: TOTO </u></em></strong><strong><em><u>and TOYOX Com. Ltd. Apr /200</u></em></strong><strong><em><u>9- </u></em></strong><strong><em><u>October 200</u></em></strong><strong><em><u>9</u></em></strong><br>
  <strong>TOTO  Com. Ltd. </strong><br>
  Recently,  shower hose (PVC) become hard and brittle throughout the <strong>Shizuoka and Kumamoto Prefecture, Japan</strong>. No reason has been identified for this phenomenon. However, I am  the first investigator, requested by hose manufacturing company (<strong>TOTO Com. Ltd.</strong>) to investigate this new  intricate damage problem. This investigation research is based on the assessment carried out by analytical  and material characterization methods. The purposes of this study are to  identify the unknown chemical changes in damaged shower hoses and clarify the  effect of water sources on the damage mechanism on the basis of the quality of Shizuoka and Kumamoto  water<br>
  TOYOX Com. Ltd. <br>
  Recently, there have been many reports  of silicon rubber (SR) hoses becoming brittle in juice factory in China (Hung-Zhou), within one month of purchase. We have collected the damaged hoses attached  to UHT sterilizer (ultra-high-temperature) in juice  factory and examined them for chemical changes. </p>
<pre><strong><em><u>&nbsp;</u></em></strong></pre>
<pre><strong><em><u>PhD Research:</u></em></strong> <strong>&ldquo;Preparation of Visible Light-active Iron-Doped TiO2 Catalyst for waste water treatment&rdquo;</strong></pre>
<pre>To apply visible light to the degradation of environmental pollutants ( phenol) was determined using HPLC and UV-visible diffuse reflectance spectra with <strong>Fe-doped</strong> as well as <strong>Fe(III)-added TiO2 </strong>suspensions was studied and the developed of Fe(III)-added TiO2 suspension-type photoreactor was applied to a electrolytes-containing phenol solution.</pre>
<pre><em>1.<strong> </strong>Preparation and characterization of Fe-doped TiO2 </em> </pre>
<p>Fe-doped  TiO2 was prepared by the calcination of FeXTiS2  (x = 0, 0.002, 0.005, 0.008, 0.01) and characterized by X-ray diffraction  (XRD), X-ray photoelectron spectroscopy (XPS), and UV-visible diffuse  reflectance spectra. All the Fe-doped TiO2 were composed of an  anatase crystal form and showed red shifts to a longer wavelength. The activity  of the Fe-doped TiO2 for the degradation of phenol was investigated  by varying the iron content during UV (365 nm) and visible light (405 nm and  436 nm) irradiation. The degradation rate depended on the Fe content and the  Fe-doped TiO2 was responsive to the visible light as well as the  elevated activity toward UV light. <br>
  <em>2. Photdegradation of water pollutant with Fe-addedTiO2</em><br>
  The combined photocatalytic system (P25 TiO2  + Fe(ClO4)3) was visible light active and the phenol  degradation rate was higher than the sum of the rates for the separately used  single photocatalyst. The reason was discussed based on the catalytic cycle of  Fe(III)/Fe(II). The suspended TiO2 particle was easily  sedimented by neutralizing the suspension. <br>
  <br>
  <strong><em><u>M. Phil. Thesis</u></em></strong><strong><u>:</u></strong><strong>&ldquo;<em>Studies on electrochemically synthesized  conducting polymer</em></strong><strong><em>&rdquo;</em></strong> <br>
  Polyanilines  are prepared by electrochemical oxidative polymerzation of aniline at different  bath composition, temperature and current density. Electrochemical depositions  were carried out at 5�C,  9�C, 18�C and 25�C  on Pt-electrode. The diposition current densities were 5 mA/cm2, 10  mA /cm2 and 15 mA/cm2. Optimal conditions for the  electrosynthesis of polyaniline were also determined. IR, X-RD and elemental  analysis characterized the polymers obtained in this studied under different  conditions. The d.c.  conductivity and UV-Vis spectroscopy measurements were performed to have an  insight in the electrical and optical properties of the studied polyaniline.  Relatively lower temperature, 10 mA/cm2 current density and H2SO4  electrolyte are found suitable for the synthesis of better quality of  conductive polyaniline. </p>
<pre>&nbsp;</pre>
<p><em><u>Master's  Thesis</u></em><u>:</u> <em>&ldquo;Sodium Sulfide  roasting of waste reformer nickel catalyst and subsequent </em><em>leaching with sulfuric acid&rdquo;</em><br>
  The  dissolution of nickel from waste powdered nickel (-200 mesh) catalyst of  fertilizer plants in sulfuric acid has been investigated as function of time,  roasting temperature and acid concentration, &quot;Waste catalyst: Na2S  &quot; weight ratio, leaching temperature and time. About 99.26 % nickel is  dissolved from the roasted mass in the leached solution within 80 minutes of  leaching, using 10N H2SO4 at the temperature of 1400  C. The dissolution of nickel (II) increased linearly with increase of roasting  temperature and leaching temperature. The optimum value of catalyst: Na2S  ratio in roasted mass is 1:2 and the percentage of dissolution was  maximum for 10N H2SO4 acid. Results indicated that the  process might be applied for the commercial dissolution of nickel from  refractory waste catalyst to produce nickel compounds. </p>
</div>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
  <tr>
    <td height="35" colspan="3" align="center" bgcolor="#0099FF" class="style3"><?php 
		include('footer.php');
	?>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
</table>
</div>
 </body>
</html>
