<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome to Dr. Shamsun Nahar</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
 </head>

<body>
<div  class="bdy">
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><a class="thumbnail" href="#thumb"><img src="images/name.jpg" alt="Dr. MST. Shamsun Nahar" border="0" /><span class="style3"><img src="images/nahardr.jpg" alt="Dr. MST. Shamsun Nahar"  /><br />Dr. MS Nahar</span></a></td>
  </tr>
  <tr>
    <td colspan="3" align="right"><hr style="color:#00CCFF; padding:1px; height:5px; background-color:#00CCFF;" />
    	<?php 
			include('main_nav.php');
		?>
    </td>
  </tr>
  <tr>
    <td width="126" align="center"><img src="images/nahar.jpg" alt="Dr. MST. Shamsun Nahar"  width="82" height="110" /></td>
    <td width="512">
    <?php 
		include('address.php');
	?>
</td>

    <td width="142" rowspan="2" align="center"></td>
  </tr>
  
  
  
  <tr>
    <td colspan="2" align="center"><p align="left" class="style3"><strong>OBJECTIVE </strong> <br />
      Looking for <strong>Material</strong> Science, <strong>Analytical </strong>and <strong>Environmental Engineering</strong> related position where I can use my skill and work experience to meet my  fields.</p>
      <p align="left" class="style3">Sex:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Female<br />
        Nationality:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Bangladeshi <br />
        Present  address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Toyamashi, Gofuku 1834-8, <br />
    Shingoso 110, Toyama 930-0887, Toyama, <strong>Japan</strong></p></td>
  </tr>
  
  <tr>
    <td colspan="3" align="center"><hr /></td>
  </tr>
  
  <tr>
    <td colspan="2" align="center"><div align="left"><u><span class="style4">List of publication</span></u></div></td>
    <td width="142" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><p align="left" class="style5">Articles</p>
      <div align="left">
        <ul class="style3">
          <li><u>Mst. Shamsun Nahar</u>, Kiyoshi Hasegawa, Shigehiro Kagaya,  and Shigeyasu Kuroda, Adsorption and Aggregation of Iron-Hydroxy Complexes  During the Photodegradation of Phenol Using the Iron-Added-TiO2  Combined System, J. Hazard. Mater Vol.162, pp. 351&ndash;355, February 2009. </li>
          <li><u>Mst.  Shamsun Nahar</u>,  Kiyoshi Hasegawa, Shigehiro Kagaya, and Shigeyasu Kuroda, Comparative  Assessment of the Efficiency of Fe-doped TiO2 Prepared by Two Doping  Methods and Photocatalytic Degradation of Phenol in Domestic Water Suspensions,  Sci.&nbsp; Technol. Adv. Mater., Vol. 8<strong>, </strong>pp<strong>.</strong> 286-291, May 2007. </li>
          <li><u>Mst.  Shamsun Nahar</u>,  Kiyoshi Hasegawa, Shigehiro Kagaya, and Shigeyasu Kuroda, Degradation of Phenol  under Visible Light Irradiation using TiO2 with Fe(III) and Easy Sedimentation of TiO2 particle, Bull.  Chem. Soc. Jpn., Vol. 80, pp. 1017-1019, May 2007. </li>
        </ul>
      </div>      </td>
  </tr>
  
  
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
  <tr>
    <td height="35" colspan="3" align="center" bgcolor="#0099FF">
    <?php 
		include('footer.php');
	?>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
</table>
</div>
 </body>
</html>
