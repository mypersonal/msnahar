<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Dr. Shamsun Nahar</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
 </head>

<body>
<div  class="bdy"><table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><a class="thumbnail" href="#thumb"><img src="images/name.jpg" border="0" /><span class="style3"><img src="images/nahardr.jpg" /><br />Dr. MS Nahar</span></a></td>
  </tr>
  <tr>
    <td colspan="3" align="right"><hr style="color:#00CCFF; padding:1px; height:5px; background-color:#00CCFF;" />
    <?php 
			include('main_nav.php');
		?>
    </td>
  </tr>
  <tr>
    <td width="126" align="center"><img src="images/nahar.jpg" width="82" height="110" /></td>
    <td width="512"> <?php 
		include('address.php');
	?></td>
    <td width="142" rowspan="2" align="center"></td>
  </tr>
  
  
  
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="3" align="center"><hr /></td>
  </tr>
  
  <tr>
    <td colspan="3" align="center" class="style3"><div align="left"><strong class="style4"><u>Educational Qualification :</u></strong><strong><u> </u></strong></div></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><div align="left">
      <p><strong>Ph. D</strong> in Material Science and Engineering, Faculty of  Engineering, University of Toyama,   Japan, March  2007.<br />
        M. Phil (Masters of Philosophy) in Chemistry from BUET (Bangladesh University  of Engineering and Technology), Bangladesh, 2001.<br />
  <strong><br />
  M. Sc</strong> (Masters)  Applied Chemistry &amp; Chemical Technology, Faculty of Engineering, Rajshahi University, Bangladesh, 1993, securing First Class.<br />
  <br />
  <strong>B. Sc</strong> (Hon&rsquo;s) in Applied Chemistry &amp; Chemical Technology (Material science), Faculty  of Engineering, Rajshahi University, Bangladesh, 1991, securing upper 2nd  Class.</p>
      <br />
    </div></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
  <tr>
    <td height="35" colspan="3" align="center" bgcolor="#0099FF" class="style3">
    <?php 
		include('footer.php');
	?>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
</table>
</div>
 </body>
</html>
