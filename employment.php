<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Dr. Shamsun Nahar</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
 </head>
<body>
<div  class="bdy">
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><a class="thumbnail" href="#thumb"><img src="images/name.jpg" border="0" /><span class="style3"><img src="images/nahardr.jpg" /><br />Dr. MS Nahar</span></a></td>
  </tr>
  <tr>
    <td colspan="3" align="right"><hr style="color:#00CCFF; padding:1px; height:5px; background-color:#00CCFF;" />
    <?php 
			include('main_nav.php');
		?>
    </td>
  </tr>
  <tr>
    <td width="126" align="center"><img src="images/nahar.jpg" width="82" height="110" /></td>
    <td width="512"> <?php 
		include('address.php');
	?></td>
    <td width="142" rowspan="2" align="center"></td>
  </tr>
  
  
  
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="3" align="center"><hr /></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><div align="left"><u class="style4">Employment</u></div></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><p align="left"><strong><u>Previous  Employment (Start from the latest one)</u> </strong></p>
      <table border="1" cellspacing="0" cellpadding="0" width="659">
  <tr>
    <td width="170" valign="top"><p align="center"><strong>Name of    Institution</strong><strong> </strong></p>
      <p>University of Toyama </p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>University of Toyama </p>
      <p>&nbsp;</p>
      <p>Dhaka College    (under National     University) </p>
      <p>&nbsp;</p>
      <p>Sreenagar Govt. College </p>
      <p>&nbsp;</p>
      <p>Bangladesh University of Eng. &amp; Tech. </p></td>
    <td width="142" valign="top"><p align="center"><strong>Location</strong></p>
      <p>Toyama, Japan </p>
      <p align="center">&nbsp;</p>
      <p align="center">&nbsp;</p>
      <p align="center">Toyama, Japan </p>
      <p align="center">&nbsp;</p>
      <p align="center">Dhaka, Bangladesh </p>
      <p align="center">&nbsp;</p>
      <p align="center">Dhaka, Bangladesh </p>
      <p align="center">&nbsp;</p>
      <p align="center">Dhaka, Bangladesh </p></td>
    <td width="180" valign="top"><p align="center"><strong>Position</strong></p>
      <p>Post doctoral Researcher<br>
         <br>
        Post doctoral Researcher<br>
        Joint    work with TOTO and TOYOX Co.LTD</p>
      <p>16th Bangladesh    Civil Service Officer (Lecturer in Chemistry)</p>
      <p>16th Bangladesh    Civil Service Officer,<br>
        Lecturer (Chemistry) </p>
      <p>Teaching    Assistance</p></td>
    <td width="168" valign="top"><p align="center"><strong>Time Length</strong></p>
      <p>October /2009- Mar /2011</p>
      <p>&nbsp;</p>
      <p>Apr /2009-    October 2009</p>
      <p>&nbsp;</p>
      <p>Dec /1997-    Jan /2002</p>
      <p>&nbsp;</p>
      <p>Aug /1996 ~ Dec /1997 </p>
      <p>&nbsp;</p>
      <p>Apr' 95 - July    96</p></td>
  </tr>
</table></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
  <tr>
    <td height="35" colspan="3" align="center" bgcolor="#0099FF" class="style3">
    <?php 
		include('footer.php');
	?>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
</table>
</div>
 </body>
</html>
