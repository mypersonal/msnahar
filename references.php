<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Dr. Shamsun Nahar</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />

 </head>

<body>
<div  class="bdy">
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><a class="thumbnail" href="#thumb"><img src="images/name.jpg" border="0" /><span class="style3"><img src="images/nahardr.jpg" /><br />Dr. MS Nahar</span></a></td>
  </tr>
  <tr>
    <td colspan="3" align="right"><hr style="color:#00CCFF; padding:1px; height:5px; background-color:#00CCFF;" />
    <?php 
			include('main_nav.php');
		?>
    </td>
  </tr>
  <tr>
    <td width="126" align="center"><img src="images/nahar.jpg" width="82" height="110" /></td>
    <td width="512"> <?php 
		include('address.php');
	?></td>
    <td width="142" rowspan="2" align="center"></td>
  </tr>
  
  
  
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="3" align="center"><hr /></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><div align="left" class="style4"><strong>References</strong></div></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><br />
<p align="left">1. <strong>Shigeyasu  Kuroda</strong><strong>, </strong><strong>Professor</strong><strong> (Ex. Supervisor)</strong><br />
      Department of Material Science and <br />
      Engineering, University of Toyama, <br />
      Toyama-shi,Gofuku 3190, <br />
      Toyama,&#12306;930-8555,  Japan </p>
      <div align="left">
        <p>Tel. &amp; Fax: +81 076 445 6819</p>
        <p>E-mail: <a href="mailto:kuro@eng.u-toyama.ac.jp">kuro@eng.u-toyama.ac.jp</a></p>
        <p>&nbsp;</p>
      </div>
      <p align="left">2. <strong>M. Fakhrul Islam, Ph.D.</strong><strong> </strong>Professor (Emeritus) <br />
        Department of Applied  Chemistry &amp; Chemical Technology,<br />
        Rajshahi University,  Bangladesh <br />
        Tel:  +(880) 721-750041 Ext. 4106 (off) <br />
        88-01912406860 (mobile) <br />
  <a href="http://www.changemakers.net/files/Resume%20Professor%20M.%20Fakhrul%20Islam.doc">http://www.changemakers.net/files/Resume%20Professor%20M.%20Fakhrul%20Islam.doc</a> </p>
      <br />
<div align="left">
        
        <p>3. <strong>Kagaya, Shigehiro, Associate Professor</strong></p>
      </div>
      <p align="left">Environmental Applied Chemistry <br />
        Engineering, University of Toyama, <br />
        Toyama-shi,Gofuku 3190, <br />
        Toyama,&#12306;930-8555,  Japan <br />
        Tel. &amp; Fax: +81 076 445 6865<br />
        E-mail: <a href="mailto:kagaya@eng.u-toyama.ac.jp">kagaya@eng.u-toyama.ac.jp</a><br />
        <br />
      </p>
      <p align="left">4. <strong>Professor  R.K. Biswas</strong><br />
        Department of Applied Chemistry and Chemical Technology, <br />
        University of&nbsp; Rajshahi, Rajshahi-6205, Bangladesh.<br />
        Tel: +(880)  721-750041 Ext. 4106 (off)<br />
        Fax: +880-721-750064<br />
        E-mail: <a href="mailto:rkbiswas53@yahoo.com">rkbiswas53@yahoo.com</a><br />
      </p></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
  <tr>
    <td height="35" colspan="3" align="center" bgcolor="#0099FF" class="style3"><?php 
		include('footer.php');
	?></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="style3"><hr /></td>
  </tr>
</table>
</div>
 </body>
</html>
